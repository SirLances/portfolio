#include <iostream>

using namespace std;

int main()
{
    int userDollars;
    int userCoins;

    cout << "Enter dollars." << endl;
    cin >> userDollars;

    cout << "Amount in dollars:" << endl;
    cout << userDollars / 20 << " Twenties." << endl;
    cout << userDollars % 20 / 10 << " Tens." << endl;
    cout << userDollars % 10 / 5 << " Fives." << endl;
    cout << userDollars % 5 / 1 << " Ones." << endl;

    cout << endl;
    cout << "Enter coins." << endl;
    cin >> userCoins;

    cout << "Amount in coins:" << endl;
    cout << userCoins / 25 << " Quarters." << endl;
    cout << userCoins % 25 / 10 << " Dimes." << endl;
    cout << userCoins % 10 / 5 << " Nickels." << endl;
    cout << userCoins % 5 / 1 << " Pennies." << endl;

    return 0;
}
