#include <iostream>
#include <string>
#include <cstring>

using namespace std;

int main()
{
    string userWord; //User inputs a word.
    string userLetter; //User inputs a letter to be found.
    int userChoice; //User inputs a choice of one of the five options.
    int letterFind; //Used to find a letter.

    cout << "Enter a Five Letter Word: ";
    cin >> userWord; //User inputs a five letter word.

    if (userWord.size() > 5) {
        cout << "ERROR: MORE THAN FIVE LETTERS." << endl; //Prevents strings with more than five letters.
    }

    else if (userWord.size() < 5) {
        cout << "ERROR: LESS THAN FIVE LETTERS." << endl; // Prevents strings with less than five letters.
    }

    else { //When user has input a five letter word.

        cout << endl; //These are the options the user has to choose from. Each numbered 1-5, where the user has to input on the next line.
        cout << "Enter your choice of operations:" << endl;
        cout << "(1)- print first and last character" << endl;
        cout << "(2)- print string containing last three characters" << endl;
        cout << "(3)- is the first letter capitalized?" << endl;
        cout << "(4)- is the letter there?" << endl;
        cout << "(5)- replace 'a' with 'A'" << endl << endl;

        cin >> userChoice; //user's choice to perform a function.
        switch (userChoice) { //Switch that allows for a choice of an above option.
            case 1: //The first case and first function.
                cout << "The first letter is " << userWord.at(0) << endl << "The last letter is " << userWord.at(4) << endl;
                break;
            case 2: //Second case and function.
                cout << userWord.at(2) << userWord.at(3) << userWord.at(4) << endl;
                break;
            case 3: //Third case and function.
                if (isupper(userWord.at(0))) { //An if/else statement that detects whether the first letter is capitalized or not.
                    cout << "First letter is capitalized." << endl;
                }
                else {
                    cout << "First letter is not capitalized." << endl;
                }
                break;
            case 4: //Fourth case and function.
                cout << "Enter a letter to be found" << endl; //Asks for the user to input a letter to be found.
                cin >> userLetter; //User's input for a letter to be found.
                if (userWord.find(userLetter) == string::npos) {
                    //Finds the letter, and if it is not there, it will say 'Letter is not here.'.
                    cout << "Letter is not here." << endl;
                }
                else {
                    cout << "Letter is there." << endl; //Says 'Letter is there.' if the letter is found.
                }
                break;
            case 5: //Fifth case and function.
                letterFind = userWord.find('a'); //Defines letterFind to find the lowercase letter 'a'.
                if (userWord.find('a') != string::npos) { //If it finds 'a', the program will capitalize it.
                    userWord[letterFind] = toupper(userWord[letterFind]);
                    cout << userWord << endl;
                }
                else { //If it does not find 'a', it will say so.
                    cout << "There is no 'a'" << endl;
                }
                    break;
        }
    }

    return 0;
}
