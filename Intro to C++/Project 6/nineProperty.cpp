#include <iostream>

using namespace std;

int main()
{
    /*
    Joe Bergeron
    */

    int fiveDigit;
    int sum;
    bool run;
    string continu;

    cout << "Would you like the play a game, Mr Anderson?" << endl;
    do { // do/while loop to find out if the user wants to play
       cout << "If yes enter 'y' if no enter 'n': ";
       cin >> continu; // user enters if they want to continue.
    } while (continu != "y" && continu != "n"); //Makes sure the user answered with a valid response.
    if (continu == "y") {// checks if they said 'y' or 'n'
        run = true;
    }
    else {
        run = false;
    }
    while (run) { //While loop to run the game over again.
        cout << "Enter a five digit number: ";
        cin >> fiveDigit;

        while (fiveDigit < 10000 || fiveDigit > 99999) { //nested while loop to get the user to enter a five digit number
            cout << "Not a five digit number" << endl;

            cout << endl << "Enter a five digit number: ";
            cin >> fiveDigit; // user enters a five digit number
        }
        sum = (fiveDigit%10) + ((fiveDigit/10)%10) + ((fiveDigit/10/10)%10) + ((fiveDigit/10/10/10)%10) + ((fiveDigit/10/10/10/10)%10);
        //Adds each digit together.
        cout << endl << "The sum of each digit is " << sum << "." << endl << endl;

        if ((sum % 9) == 0 && (fiveDigit % 9) == 0) { //if statement to find out if it worked or not
            cout << "It worked " << endl;
        }
        else {
            cout << "Doesn't work " << endl;
        }

        cout << "Would you like to play again?" << endl;

        do { //do/while loop to see if the user wants to play again.
            cout << "If yes enter 'y' if no enter 'n': ";
            cin >> continu; //user inputs their answer.
        } while (continu != "y" && continu != "n"); //Sees if the user input either 'y' or 'n'

        if (continu == "y") { // checks if the user said 'y' or 'n'
            run = true;
        }
        else {
            run = false;
        }
    }

    return 0;
}
