#include <iostream>

using namespace std;

int main()
{
    // Joe Bergeron
    string firstName;
    string lastName;
    //Variables for names
    string movieName; //Movie's name
    int numTickets; //Number of tickets
    double ticketPrice = 5.50; //Price of tickets

    cout << "Welcome to Ticket Master." << endl;
    cout << "Please enter your full name." << endl; // Asks for name.
    cin >> firstName; //user input of first name
    cin >> lastName; //user input of second name
    cout << "What movie do you want to see?" << endl;
    cin >> movieName; //user input for movie name
    cout << "How many tickets?" << endl;
    cin >> numTickets; //user input for the number of tickets
    cout << lastName << " " << firstName << "   " << movieName << endl;
    cout << "Your price is $" << ticketPrice * numTickets << endl;
    cout << "DISCOUNT: Each 3rd ticket free." << endl;
    cout << "Your final price is $" << ticketPrice * (numTickets - (numTickets / 4)) << endl;
    /*
    * The math takes the user input and divides it by 4.
    * That is then subtracted by the input again.
    * Finally it is multiplied by the ticket price.
    */

    return 0;
}
