#include <iostream>

using namespace std;

int main()
{
    int pocketNum;

    cin >> pocketNum;

    if (pocketNum == 0) {
        cout << "Pocket is green." << endl;
    }
    else if ((pocketNum == 1) || (pocketNum == 3) || (pocketNum == 5) || (pocketNum == 7) || (pocketNum == 9)) {
        cout << "Pocket is red." << endl;
    }
    else if ((pocketNum == 2) || (pocketNum == 4) || (pocketNum == 6) || (pocketNum == 8) || (pocketNum == 10)) {
        cout << "Pocket is black." << endl;
    }
    else if ((pocketNum == 11) || (pocketNum == 13) || (pocketNum == 15) || (pocketNum == 17)) {
        cout << "Pocket is black." << endl;
    }
    else if ((pocketNum == 12) || (pocketNum == 14) || (pocketNum == 16) || (pocketNum == 18)) {
        cout << "Pocket is red." << endl;
    }
    else if ((pocketNum == 19) || (pocketNum == 21) || (pocketNum == 23) || (pocketNum == 25) || (pocketNum == 27)) {
        cout << "Pocket is red." << endl;
    }
    else if ((pocketNum == 20) || (pocketNum == 22) || (pocketNum == 24) || (pocketNum == 26) || (pocketNum == 28)) {
        cout << "Pocket is black." << endl;
    }
    else if ((pocketNum == 29) || (pocketNum == 31) || (pocketNum == 33) || (pocketNum == 35)) {
        cout << "Pocket is black." << endl;
    }
    else if ((pocketNum == 30) || (pocketNum == 32) || (pocketNum == 34) || (pocketNum == 36)) {
        cout << "Pocket is red." << endl;
    }
    else {
        cout << "ERROR 404: Number not found." << endl;
    }
    return 0;
}
