#include <iostream>
#include <vector>

using namespace std;


void getDegree(double& degree);
void getScore(double scores[], int numScores);
double sum(const double scores[], int numScores);
double highest(const double scores[], int numScores);
double lowest(const double scores[], int numScores);
double average(vector<double> finals);

int main()
{
    double degree;
    double scores[7];
    double sumTotal;
    vector<double> finalScores;
    double finishedScore;
    int nextDive = 0;

    do {
        getDegree(degree);
        getScore(scores, 7);
        sumTotal = sum(scores, 7) - highest(scores, 7) - lowest(scores, 7);
        cout << sumTotal << endl;
        finishedScore = sumTotal * degree * 0.6;
        cout << finishedScore << endl;
        finalScores.push_back(finishedScore);
        cout << "Is there another diver?" << endl << "0 for no, 1 for yes" << endl;
        cin >> nextDive;
    } while (nextDive == 1);
    for (int i = 0; i < (int) finalScores.size(); ++i) {
        cout << finalScores.at(i) << ", ";
    }
    cout << endl << average(finalScores);

    return 0;
}

void getDegree(double& degree)
{
    do {
        cout << "Enter a degree of difficulty." << endl;
        cin >> degree;
    } while (degree < 1.2 || degree > 3.8);
}
void getScore(double scores[], int numScores) {
    cout << "Enter " << numScores << " scores."  << endl;
    for (int i = 0; i < numScores; ++i) {
        cin >> scores[i];
    }
}
double sum(const double scores[], int numScores){
    double total = 0;
    for(int i = 0; i < numScores; ++i) {
        total = total + scores[i];
    }
    return total;
}
double highest(const double scores[], int numScores) {
    double big = scores[0];
    for (int i = 0; i < numScores; ++i) {
        if (scores[i] > big) {
            big = scores[i];
        }
    }
    return big;
}
double lowest(const double scores[], int numScores){
    double small = scores[0];
    for (int i = 0; i < numScores; ++i) {
        if (scores[i] < small) {
            small = scores[i];
        }
    }
    return small;
}
double average(vector<double> finals){
    double num = finals.size();
    double total = 0;
    for (int i=0; i < num; ++i) {
        total = total + finals.at(i);
    }
    return total / num;
}

