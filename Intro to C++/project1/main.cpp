#include <iostream>

using namespace std;

int main()
{
     // Joe Bergeron
    int userAge;

    cout << "XXXXXXXXX" << endl; // The letter J written with X's.
    cout << "   XX" << endl;
    cout << "   XX" << endl;
    cout << "   XX" << endl;
    cout << "XX XX" << endl;
    cout << "XXXXX" << endl;
    cout << endl;
    cout << "Enter your age " << endl;
    cin >> userAge; // user inputs age.
    cout << "You will be " << userAge + 5 << " in five years."; // five is added to the inputted age.

    return 0;
}
