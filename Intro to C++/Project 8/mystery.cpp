
#include <iostream>
#include <vector>
#include <string>

using namespace std;

//Joe

int main()
{
    const int RES_NUM = 50; //Makes the number of reservations constantly 50.
    vector<string> resCustom(RES_NUM, "{empty}"); //Vector for reservations.
    vector<string> waitCustom; //Vector for wait list.
    string customName; //Customers user-inputed name.
    int userInput; //Input for what the user chooses to do.
    int opEnd; //End the program.
    int addWait = 0; //Used for if statement to add customer to the wait list.
    int resLook = 0; //Used if name isn't in reservation.
    unsigned int i; //blank variable for For loops.

    do {
        cout << endl;
        cout << "What would you like to;" << endl; //Asks user what operation they want to do.
        cout << "(1) Make a reservation." << endl;
        cout << "(2) See if you already have a reservation." << endl;
        cout << "(3) See all who have reservations." << endl;
        cout << "(4) See all who are on the wait list." << endl;
        cout << "(5) End operations." << endl;

        cin >> userInput; //User inputs the number they want to do.
        switch (userInput) { //Switch for all the operations.
            case 1: //Let's user make a reservation.
                cout << "Customer's Name: ";
                cin >> customName;
                for (i = 0; i < RES_NUM; ++i) { //Searches for whether or not there is an empty space.
                    if (resCustom.at(i) == "{empty}") {
                        resCustom.at(i) = customName;
                        cout << "You now have a reservation set for " << customName << "." << endl;
                        addWait = 1;
                        break;
                    }
                    }
                if (addWait == 0) { //If there isn't an empty space, they are added to the wait list.
                    waitCustom.push_back(customName);
                    cout << customName << " has been added to the wait list." << endl;
                }
                addWait = 0;
            break;
            case 2: //Let's user see if they have a reservation or are on the wait list.
                cout << "Customer Name: ";
                cin >> customName;
                for (i = 0; i < RES_NUM; ++i) { //Searches to see if they have a reservation.
                    if (resCustom.at(i) == customName) {
                        cout << "Yes, there is a reservation for " << customName << "." << endl;
                        resLook = 1;
                        break;
                    }
                }
                for (i = 0; i < waitCustom.size(); ++i) { //Searches to see if they are on the wait list.
                    if (waitCustom.at(i) == customName) {
                        cout << customName << "is on the wait list." << endl;
                        resLook = 1;
                    }
                }
                if (resLook == 0) { //If they aren't on either, this promps.
                    cout << "Sorry, " << customName << " doesn't have a reservation." << endl;
                }
            break;
            case 3: // Shows the list of reservations.
                cout << "Reservations;" << endl;
                for (i = 0; i < RES_NUM; ++i) { //Writes all of the people in reservation or if there is an empty space.
                    if (i < RES_NUM - 1) {
                        cout << resCustom.at(i) << ", ";
                    }
                    else {
                        cout << resCustom.at(i) << "." << endl;
                    }
                }
            break;
            case 4: // Shows the wait list.
                cout << "Wait-listed;" << endl;
                for (i = 0; i < waitCustom.size(); ++i) { //Writes all the people on the wait list.
                    if (i < waitCustom.size() - 1) {
                    cout << waitCustom.at(i) << ", ";
                    }
                    else {
                        cout << waitCustom.at(i) << "." << endl;
                    }
                }
            break;
            case 5: // Ends program.
                cout << "Ending operations." << endl;
                opEnd = 1;
            break;
        }
    } while (opEnd != 1); //If the user chooses 5, it ends the program.

    cout << "Goodbye" << endl; // Just a farewell message.

    return 0;

}
