#include <iostream>
#include <string>


//Joe Bergeron
int main()
{
	std::string userString;
	const int NUM_SEATS = 20; //total number of seats
	int totalSeats[NUM_SEATS]; //array for all the seats
	std::string run; //if user wants to initialize all seats to empty
	bool toAgain = true; //to see if user wants to do another option
	bool foundEmpty; //find if a seat is empty
	int seatCount; //number of seat
	int seatChoice; //users choice of seat
	int userOption; // users choice for option
	do {
		std::cout << "Enter 'yes' to make all seats empty: ";
		//user confirmation to initialize seats to 0

		std::cin >> run;
	} while (run != "yes");
	for (int i = 0; i < NUM_SEATS; ++i)
	{
		totalSeats[i] = 0;
	}
	do
	{
		//user choice part
		std::cout << "What would you like to do?" << std::endl;
		std::cout << "(1) reserve a particular seat, if available" << std::endl;
		std::cout << "(2) reserve a seat furthest to the front" << std::endl;
		std::cout << "(3) reserve a seat furthest to the rear" << std::endl;
		std::cout << "(4) cancel a particular seat reservation" << std::endl;
		std::cout << "(5) print out the status of all seats on the train" << std::endl;
		std::cout << "(6) check if train is full" << std::endl;
		std::cout << "(7) check if train is empty" << std::endl;
		std::cout << "(8) exit" << std::endl;
		std::cin >> userOption;
		switch (userOption) {
		case 1:
			//reserve a particular seat, if seat is available
			std::cout << "What seat would you like to reserve? From 0-19: ";
			std::cin >> seatChoice;
			if (totalSeats[seatChoice] == 0){
				std::cout << "Seat " << seatChoice << " has been reserved";
				totalSeats[seatChoice] = 1;
			}
			else if (totalSeats[seatChoice] == 1){
				std::cout << "Seat is already full" << std::endl;
			}
			else{
				std::cout << "Invalid seat number" << std::endl;
			}
			break;
		case 2:
			//reserve the seat furthest to the front that is available
			foundEmpty = false;
			for (int i = 0; i < NUM_SEATS; ++i){
				if (totalSeats[i] == 0 && foundEmpty == false){
					totalSeats[i] = 1;
					foundEmpty = true;
					std::cout << "The seat that is closest to the front and empty is seat "<< i << " and has been reserved." << std::endl;
				}
			}
			if (foundEmpty == false){
				std::cout << "No empty seats found" << std::endl;
			}
			break;
		case 3:
			//reserve the seat furthest to the rear that is available
			foundEmpty = false;
			for (int i = NUM_SEATS-1; i >= 0; --i){
				if (totalSeats[i] == 0 && foundEmpty == false){
					totalSeats[i] = 1;
					foundEmpty = true;
					std::cout << "The seat that is closest to the rear and empty is seat " << i << " and has been reserved" << std::endl;
				}
			}
			if (foundEmpty == false){
				std::cout << "No empty seats found" << std::endl;
			}
			break;
		case 4:
			//cancel a particular seat reservation
			std::cout << "What seat would you like to cancel (0-19): ";
			std::cin >> seatChoice;
			if (totalSeats[seatChoice] == 1){
				std::cout << "Seat " << seatChoice << " has had its reservation cancled";
				totalSeats[seatChoice] = 0;
			}
			else if (totalSeats[seatChoice] == 0){
				std::cout << "Seat is already empty" << std::endl;
			}
			else{
				std::cout << "Invalid seat number" << std::endl;
			}
			break;
		case 5:
			//prints the status of all the seats on the train
			for (int i = 0; i < NUM_SEATS; ++i){
				std::cout << "Seat " << i;
				if (totalSeats[i]==0){
					std::cout << " is empty" << std::endl;
				}
				else{
					std::cout << " is occupied" << std::endl;
				}
			}
			break;
		case 6:
			//check to see if the train is full
			seatCount = 0;
			for (int i = 0; i < NUM_SEATS; ++i){
				if (totalSeats[i] == 1){
					++seatCount;
				}
			}
			if (seatCount == 20){
				std::cout << "All seats are full" << std::endl;
			}
			else{
				std::cout << "There is at least 1 empty seat" << std::endl;
			}
			break;
		case 7:
			//check to see if the train is empty
			seatCount = 0;
			for (int i = 0; i < NUM_SEATS; ++i){
				if (totalSeats[i] == 0){
					++seatCount;
				}
			}
			if (seatCount == 20){
				std::cout << "All seats are empty" << std::endl;
			}
			else{
				std::cout << "There is at least 1 full seat" << std::endl;
			}
			break;
		case 8:
			//quit application
			toAgain = false;
			break;
		default:
			//if entry given is invalid
			std::cout << "Entry Invalid" << std::endl;
			break;
		}
		//user confirmation to continue as long as they are not trying to quit
		if (toAgain){
			do {
				std::cout << std::endl << "Enter 'again' to go again: ";
				//user confirmation to go again
				std::cin >> run;
			} while (run != "again");
		}
	} while (toAgain); //finds whether user wants to find another option or end program
	return 0;
}
