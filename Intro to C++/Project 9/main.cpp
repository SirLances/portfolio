#include <iostream>
#include <string>

using namespace std;

//Joe Bergeron

int getFeet()//this prompts the user to give a positive int number of feet

{
	int f = -1;
	do{
		cout << "Enter a valid number of feet (must be a positive number): ";
		cin >> f;
	} while (f < 0);
	return f;
}
//this prompts the user to give a positive int number of inches
int getInches()
{
	int i = -1;
	do {
		cout << "Enter a valid number of inches (must be a positive number): ";
		cin >> i;
	} while (i < 0);
	return i;
}
//this prompts the user to give a positive int number of pounds
int getPounds()
{
	int p = -1;
	do {
		cout << "Enter a valid number of pounds (must be a positive number): ";
		cin >> p;
	} while (p < 0);
	return p;
}
//this prompts the user to give a positive int number of ounces
int getOunces()
{
	int o = -1;
	do {
		cout << "Enter a valid number of ounces (must be a positive number): ";
		cin >> o;
	} while (o < 0);
	return o;
}

void MetricDistance(int feet, int inches) //converts feet + inches to meters + centimeters.
{
	cout << feet << " feet & " << inches << " inches converts to ";
	int centimeters = (((feet * 12) + inches)*2.54);
	int meter = centimeters / 100;
	centimeters = centimeters % 100;
	cout << meter << " meters & " << centimeters << " centimeters"<<endl;
}

void MetricWeight(int pounds, int ounces)//converts pounds + ounces to kilograms + grams.
{
	cout << pounds << " pounds & " << ounces << " ounces converts to ";
	int grams = (((pounds * 16) + ounces)*28.3495);
	int kilograms = grams / 1000;
	grams = grams % 1000;
	cout << kilograms << " kilograms & " << grams << " grams" << endl;
}


int main()
{
	int userOption;
	bool toStop = false;
	string run;
	do
	{
		//user choice part
		cout << "Enter your choice of operation:" << endl;
		cout << "1- convert U.S. distance to metric distance" << endl;
		cout << "2- convert U.S. weight to metric weight" << endl;
		cout << "3- exit" << endl;
		cin >> userOption;
		switch (userOption) {
		case 1: //convert US distance to metric distance.
			MetricDistance(getFeet(),getInches());
			break;
		case 2: //convert US weight to metric weight.
			MetricWeight(getPounds(), getOunces());
			break;
		case 3: //quit application.
			toStop = true;
			break;
		default: //return '-1' if invalid value.
			cout << -1 << endl;
			break;
		}

    cout << endl;

	} while (toStop != true); //ends the program.
	return 0;
}
