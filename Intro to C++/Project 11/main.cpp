#include <iostream>

using namespace std;

class Customer {
public:
    void setAcct(int aNo);
    void setCredit(double amount);
    int getAcctNo() const;
    double getCreditLimit() const;
    double getBal() const;
    double availCredit() const;
    void moreCredit(double amount);
    bool makePurchase(double amount);
    void display() const;
    void makePayment(double amount);

private:
    int accountNo;
    double creditLimit;
    double balance = 0.0; //The balance of a new customer always starts at 0.0
};

void Customer::moreCredit(double amount) { //Increases the credit limit of the customer.
    creditLimit = creditLimit + amount;
}
bool Customer::makePurchase(double amount) { //Subtracts from the balance as long as there is money in the balance.
    if (amount < balance) {
        balance = balance - amount;
    }
    else {
        cout << "Not enough credit for purchase." << endl;
    }
    return balance;
}
void Customer::makePayment(double amount) { //Adds to the balance the amount paid.
    balance = balance + amount;
}
void Customer::display() const { // Displays all the info of Customer class.
    cout << getAcctNo() << endl;
    cout << getCreditLimit() << endl;
    cout << getBal() << endl;
}
void Customer::setAcct(int ano) { //Sets the account number of customer.
    accountNo = ano;
}
void Customer::setCredit(double amount) { //Sets the credit limit of the customer.
    creditLimit = amount;
}
int Customer::getAcctNo() const { //Gets the account number of the customer.
    return accountNo;
}
double Customer::getCreditLimit() const { //Gets the credit limit of the customer.
    return creditLimit;
}
double Customer::getBal() const { //Gets the balance of the customer.
    return balance;
}
double Customer::availCredit() const { //Show the available credit.
    double creditAvail = creditLimit - balance;
    return creditAvail;
}

int main()
{
    int userMonths;
    double credit;
    int acct;
    double totalPurchase;
    double addPayment;
    Customer customer1; //The object of the Customer class.

    cout << "Account Number: ";
    cin >> acct; //User enters the account number of the customer.
    customer1.setAcct(acct);

    cout << "Credit Limit: ";
    cin >> credit; //User enters the credit limit of the customer.
    customer1.setCredit(credit);

    cout << customer1.getAcctNo() << endl;
    cout << customer1.getCreditLimit() << endl;
    cout << customer1.getBal() << endl;

    cout << "How many months will be simulated?" << endl;
    cin >> userMonths;
    for (int i = 0; i < userMonths; ++i) { //Loops for as many months the user entered.
        cout << "Enter total of all purchases for the customer for month " << i + 1 << "." << endl;
        cin >> totalPurchase; //Used for the makePurchase function.
        customer1.makePurchase(totalPurchase);
        cout << endl;

        cout << "Please provide a payment for the customer for month " << i + 1 << "." << endl;
        cin >> addPayment; //Used for the makePayment function.
        customer1.makePayment(addPayment);
        cout << endl;

        cout << "Available Balance: " << customer1.getBal() << endl << endl;

        customer1.display(); //Displays the customers info.
        cout << endl << endl;
    }

    return 0;
}
